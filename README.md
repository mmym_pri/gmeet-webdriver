# What it does
Login to your Google account (2FA needs to be *disabled*), then navigate to the specified Google Meet URL.
Microphone and Camera is enabled by default, so you want to mute your mic in your OS settings. I just let camera on because I use OBS Virtual Camera as an input instead.
  
# Installation
  
This script requires: 
- selenium
- chrome webdriver
- pynput
  
Download Chrome WebDriver, then install Selenium and PyNput using `pip install`
    
# Running
  
Depending on where you put your chrome webdriver, you might need to specify it in the script. Or you can just download it to this folder, move `login.py` to where it is, etc.
  
```bash
$ python login.py <time (in hours)>
```


