#!/usr/bin/env python
'''
If you want to automate it i.e. run at specific time, etc. use a cronjob or something
'''
import time
import pynput
import sys
from selenium import webdriver
from selenium.webdriver import ActionChains
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait as wait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.keys import Keys
from pynput.keyboard import Key, Controller

#Close WebDriver automatically after certain duration
if len(sys.argv) == 1:
    print("No argument given, setting duration to 2 minutes\n\n")
    duration = 120 #2m delay by default
else:
    duration = 120+(sys.argv[1]*60*60) #2m + duration in hours

#Set Chromedriver options so that Meet doesn't whine about me not having a Microphone/Camera
opt = Options()
opt.add_argument("--disable-inforbars")
opt.add_argument("--disable-extensions")
opt.add_experimental_option("prefs", {\
    "profile.default_content_setting_values.media_stream_mic": 1, 
    "profile.default_content_setting_values.media_stream_camera": 1,
    "profile.default_content_setting_values.geolocation": 1, 
    "profile.default_content_setting_values.notifications": 1 
})

#Credentials
usernameStr = "YOUR EMAIL HERE"
passwordStr = "YOUR PASSWORD HERE"
url_meet = "GOOGLE MEET URL HERE" 

browser = webdriver.Chrome(chrome_options=opt)


#Login to Google account
browser.get(('https://accounts.google.com/ServiceLogin?'
             'service=mail&continue=https://google.com'))
username = browser.find_element_by_id('identifierId')
username.send_keys(usernameStr)
nextButton = browser.find_element_by_id('identifierNext')
nextButton.click()

time.sleep(7) #wait 7s so it doesn't enter password in user field 
keyboard = Controller()
password = browser.find_element_by_xpath("//input[@class='whsOnd zHQkBf']")
password.send_keys(passwordStr)
signInButton = browser.find_element_by_id('passwordNext')
signInButton.click()
time.sleep(3)


#Open and Join Google Meet
browser.get(url_meet)
time.sleep(7)

browser.find_element_by_xpath("//span[contains(text(), 'Join now')]").click()

time.sleep(duration)
browser.quit()
